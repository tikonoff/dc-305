﻿namespace Booking.Booking
{
    partial class AskName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameBox = new System.Windows.Forms.TextBox();
            this.findButton = new System.Windows.Forms.Button();
            this.namesView = new System.Windows.Forms.DataGridView();
            this.namesPanel = new System.Windows.Forms.Panel();
            this.selectButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.title = new System.Windows.Forms.Label();
            this.confirmLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.namesView)).BeginInit();
            this.namesPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // nameBox
            // 
            this.nameBox.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameBox.Location = new System.Drawing.Point(133, 116);
            this.nameBox.Margin = new System.Windows.Forms.Padding(2);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(240, 28);
            this.nameBox.TabIndex = 1;
            // 
            // findButton
            // 
            this.findButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.findButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.findButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.findButton.Location = new System.Drawing.Point(387, 114);
            this.findButton.Margin = new System.Windows.Forms.Padding(2);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(107, 30);
            this.findButton.TabIndex = 2;
            this.findButton.Text = "Find";
            this.findButton.UseVisualStyleBackColor = false;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // namesView
            // 
            this.namesView.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.namesView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.namesView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.namesView.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.namesView.Location = new System.Drawing.Point(16, 15);
            this.namesView.Margin = new System.Windows.Forms.Padding(2);
            this.namesView.Name = "namesView";
            this.namesView.RowTemplate.Height = 24;
            this.namesView.Size = new System.Drawing.Size(360, 122);
            this.namesView.TabIndex = 3;
            // 
            // namesPanel
            // 
            this.namesPanel.Controls.Add(this.confirmLabel);
            this.namesPanel.Controls.Add(this.selectButton);
            this.namesPanel.Controls.Add(this.namesView);
            this.namesPanel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.namesPanel.Location = new System.Drawing.Point(118, 168);
            this.namesPanel.Margin = new System.Windows.Forms.Padding(2);
            this.namesPanel.Name = "namesPanel";
            this.namesPanel.Size = new System.Drawing.Size(392, 246);
            this.namesPanel.TabIndex = 4;
            // 
            // selectButton
            // 
            this.selectButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.selectButton.Location = new System.Drawing.Point(269, 146);
            this.selectButton.Margin = new System.Windows.Forms.Padding(2);
            this.selectButton.Name = "selectButton";
            this.selectButton.Size = new System.Drawing.Size(107, 30);
            this.selectButton.TabIndex = 5;
            this.selectButton.Text = "Book";
            this.selectButton.UseVisualStyleBackColor = false;
            this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.cancelButton.ForeColor = System.Drawing.Color.Transparent;
            this.cancelButton.Location = new System.Drawing.Point(508, 114);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(107, 30);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Source Sans Pro Light", 18F, System.Drawing.FontStyle.Bold);
            this.title.Location = new System.Drawing.Point(127, 70);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(195, 31);
            this.title.TabIndex = 6;
            this.title.Text = "Customer name?";
            // 
            // confirmLabel
            // 
            this.confirmLabel.AutoSize = true;
            this.confirmLabel.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.confirmLabel.Location = new System.Drawing.Point(12, 151);
            this.confirmLabel.Name = "confirmLabel";
            this.confirmLabel.Size = new System.Drawing.Size(87, 20);
            this.confirmLabel.TabIndex = 6;
            this.confirmLabel.Text = "Movie & seats";
            // 
            // AskName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 449);
            this.ControlBox = false;
            this.Controls.Add(this.title);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.namesPanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AskName";
            this.ShowIcon = false;
            this.Text = "Customer name";
            ((System.ComponentModel.ISupportInitialize)(this.namesView)).EndInit();
            this.namesPanel.ResumeLayout(false);
            this.namesPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.DataGridView namesView;
        private System.Windows.Forms.Panel namesPanel;
        private System.Windows.Forms.Button selectButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Label confirmLabel;
    }
}