﻿namespace Booking.Booking
{
    partial class SearchBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmitSearch = new System.Windows.Forms.Button();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.SearchLabel = new System.Windows.Forms.Label();
            this.gridLabel = new System.Windows.Forms.Label();
            this.gridPanel = new System.Windows.Forms.Panel();
            this.addNewButton = new System.Windows.Forms.Button();
            this.ticketsGridView = new System.Windows.Forms.DataGridView();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.bookNowButton = new System.Windows.Forms.Button();
            this.panelAddNew = new System.Windows.Forms.Panel();
            this.labelNoBookings = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.gridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ticketsGridView)).BeginInit();
            this.panelAddNew.SuspendLayout();
            this.SuspendLayout();
            // 
            // SubmitSearch
            // 
            this.SubmitSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubmitSearch.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitSearch.Location = new System.Drawing.Point(761, 81);
            this.SubmitSearch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SubmitSearch.Name = "SubmitSearch";
            this.SubmitSearch.Size = new System.Drawing.Size(163, 46);
            this.SubmitSearch.TabIndex = 0;
            this.SubmitSearch.Text = "Search";
            this.SubmitSearch.UseVisualStyleBackColor = true;
            this.SubmitSearch.Click += new System.EventHandler(this.SubmitSearch_Click);
            // 
            // SearchBox
            // 
            this.SearchBox.Font = new System.Drawing.Font("Source Sans Pro Light", 16F);
            this.SearchBox.Location = new System.Drawing.Point(163, 84);
            this.SearchBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(572, 41);
            this.SearchBox.TabIndex = 1;
            // 
            // SearchLabel
            // 
            this.SearchLabel.AutoSize = true;
            this.SearchLabel.Font = new System.Drawing.Font("Source Sans Pro Light", 16F);
            this.SearchLabel.Location = new System.Drawing.Point(32, 87);
            this.SearchLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SearchLabel.Name = "SearchLabel";
            this.SearchLabel.Size = new System.Drawing.Size(80, 34);
            this.SearchLabel.TabIndex = 3;
            this.SearchLabel.Text = "Name";
            this.SearchLabel.Click += new System.EventHandler(this.SearchLabel_Click);
            // 
            // gridLabel
            // 
            this.gridLabel.AutoSize = true;
            this.gridLabel.Font = new System.Drawing.Font("Source Sans Pro Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gridLabel.Location = new System.Drawing.Point(16, 17);
            this.gridLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gridLabel.Name = "gridLabel";
            this.gridLabel.Size = new System.Drawing.Size(116, 34);
            this.gridLabel.TabIndex = 5;
            this.gridLabel.Text = "Bookings";
            // 
            // gridPanel
            // 
            this.gridPanel.Controls.Add(this.addNewButton);
            this.gridPanel.Controls.Add(this.ticketsGridView);
            this.gridPanel.Controls.Add(this.deleteButton);
            this.gridPanel.Controls.Add(this.updateButton);
            this.gridPanel.Controls.Add(this.gridLabel);
            this.gridPanel.Location = new System.Drawing.Point(16, 149);
            this.gridPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(1171, 400);
            this.gridPanel.TabIndex = 6;
            // 
            // addNewButton
            // 
            this.addNewButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addNewButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewButton.Location = new System.Drawing.Point(725, 289);
            this.addNewButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(183, 46);
            this.addNewButton.TabIndex = 9;
            this.addNewButton.Text = "Add new";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // ticketsGridView
            // 
            this.ticketsGridView.AllowUserToAddRows = false;
            this.ticketsGridView.AllowUserToDeleteRows = false;
            this.ticketsGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.ticketsGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ticketsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ticketsGridView.Location = new System.Drawing.Point(147, 17);
            this.ticketsGridView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ticketsGridView.Name = "ticketsGridView";
            this.ticketsGridView.Size = new System.Drawing.Size(989, 239);
            this.ticketsGridView.TabIndex = 8;
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.RosyBrown;
            this.deleteButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.deleteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Transparent;
            this.deleteButton.Location = new System.Drawing.Point(147, 289);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(161, 46);
            this.deleteButton.TabIndex = 7;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.Location = new System.Drawing.Point(923, 289);
            this.updateButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(183, 46);
            this.updateButton.TabIndex = 6;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // bookNowButton
            // 
            this.bookNowButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bookNowButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bookNowButton.Location = new System.Drawing.Point(41, 97);
            this.bookNowButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bookNowButton.Name = "bookNowButton";
            this.bookNowButton.Size = new System.Drawing.Size(199, 46);
            this.bookNowButton.TabIndex = 11;
            this.bookNowButton.Text = "Book now";
            this.bookNowButton.UseVisualStyleBackColor = true;
            this.bookNowButton.Click += new System.EventHandler(this.bookNowButton_Click);
            // 
            // panelAddNew
            // 
            this.panelAddNew.Controls.Add(this.labelNoBookings);
            this.panelAddNew.Controls.Add(this.bookNowButton);
            this.panelAddNew.Location = new System.Drawing.Point(121, 149);
            this.panelAddNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelAddNew.Name = "panelAddNew";
            this.panelAddNew.Size = new System.Drawing.Size(344, 225);
            this.panelAddNew.TabIndex = 12;
            this.panelAddNew.Visible = false;
            // 
            // labelNoBookings
            // 
            this.labelNoBookings.AutoSize = true;
            this.labelNoBookings.Font = new System.Drawing.Font("Source Sans Pro Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNoBookings.Location = new System.Drawing.Point(32, 41);
            this.labelNoBookings.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNoBookings.Name = "labelNoBookings";
            this.labelNoBookings.Size = new System.Drawing.Size(241, 34);
            this.labelNoBookings.TabIndex = 12;
            this.labelNoBookings.Text = "No current bookings.";
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.Transparent;
            this.cancelButton.Location = new System.Drawing.Point(959, 81);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(163, 46);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // SearchBooking
            // 
            this.AcceptButton = this.SubmitSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1239, 682);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.panelAddNew);
            this.Controls.Add(this.gridPanel);
            this.Controls.Add(this.SearchLabel);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.SubmitSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SearchBooking";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search booking";
            this.Load += new System.EventHandler(this.SearchBooking_Load);
            this.gridPanel.ResumeLayout(false);
            this.gridPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ticketsGridView)).EndInit();
            this.panelAddNew.ResumeLayout(false);
            this.panelAddNew.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SubmitSearch;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.Label SearchLabel;
        private System.Windows.Forms.Label gridLabel;
        private System.Windows.Forms.Panel gridPanel;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.DataGridView ticketsGridView;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rowDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn seatDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button bookNowButton;
        private System.Windows.Forms.Panel panelAddNew;
        private System.Windows.Forms.Label labelNoBookings;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn paidDataGridViewCheckBoxColumn;
        private System.Windows.Forms.Button cancelButton;
    }
}