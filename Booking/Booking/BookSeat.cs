﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Forms;

namespace Booking.Booking
{
    public partial class CheckSeats : Form
    {
        private string Name { get; set; }
        private string movieTitle;
        private List<object> Seats = new List<object>();

        public CheckSeats(string name = "")
        {
            InitializeComponent();
            Name = name;
        }

        public string connStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;

        private void BookSeat_Load(object sender, EventArgs e)
        {
            if (Name != "")
                nameLabel.Text = "Booking for " + Name;
            var dt = new DataTable(connStr);
            var connection = new SqlConnection(connStr);
            var cmd = new SqlCommand(
                "SELECT Shows.Id, Shows.Date, Movies.Title, Types.Name, Actors.Name, Ratings.Name FROM Movies "  
                + "JOIN Actors ON Actors.Id=Movies.Actor_ID "
                + "JOIN Types ON Types.Id=Movies.Type_ID " 
                + "JOIN Ratings ON Ratings.Id=Movies.Rating_ID "
                + "JOIN Shows ON Shows.Movie_ID = Movies.Id "
                + "WHERE Shows.Date >= GETDATE()"
                , connection);

            try
            {
                connection.Open();
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dt.Load(reader);
                    showGridView.DataSource = dt;
                    showGridView.Columns[1].Width = 70;
                    showGridView.Columns[2].Width = 190;
                    showGridView.Columns[3].Width = 70;
                    showGridView.Columns[3].HeaderText = "Type";
                    showGridView.Columns[4].HeaderText = "Actor";
                    showGridView.Columns[5].HeaderText = "Rating";
                    showGridView.Columns[5].Width = 50;
                    showGridView.Columns[0].Visible = false;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        private void checkButton_Click(object sender, EventArgs e)
        {
            showGridView.Visible = false;
            var showID = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[0].Value.ToString();
            const int seatsTotal = 12; // 12 seats
            const int rowsTotal = 8; // 8 rows
            var row = new object[seatsTotal];

            for (var i = 0; i < rowsTotal; i++)
            {
                for (var j = 0; j < seatsTotal; j++)
                {
                    row[j] = "";
                }
                seatsView.Rows.Add(row);
            }

            var connection = new SqlConnection(connStr);
            var cmd = new SqlCommand(
                "SELECT Seats.Seats FROM Shows " +
                "JOIN Seats ON Shows.Seats_ID = Seats.Id " +
                "WHERE  Shows.Id=" + showID
                , connection);

            try
            {
                connection.Open();
                var allSeats = cmd.ExecuteScalar();
                if (allSeats != null)  // new Seats Set should be written on first booking, not on Show creating. 
                {
                    var seatSet = allSeats.ToString().Split(';');
                    var set = new string[3];
                    foreach (string t in seatSet)
                    {
                        set = t.Split('.');
                        seatsView.Rows[Convert.ToInt32(set[0])].Cells[Convert.ToInt32(set[1])].Value = set[2];
                        seatsView.Rows[Convert.ToInt32(set[0])].Cells[Convert.ToInt32(set[1])].Style.BackColor = Color.LightSlateGray;
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            seatsPanel.Visible = true;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void backlButton_Click(object sender, EventArgs e)
        {
            Form form = new CheckSeats();
            this.Hide();
            form.MdiParent = this.ParentForm;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void seatsView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (seatsView.CurrentCell.Value != "")
            {
                MessageBox.Show("Already sold!");
            }
            else
            {
                if (seatsView.CurrentCell.Style.BackColor == Color.DarkTurquoise)
                {
                    seatsView.CurrentCell.Style.BackColor = Color.White;
                    seatsView.CurrentCell.Style.SelectionBackColor = Color.White;
                    Seats.RemoveAt((int) (Seats.Count - 1));
                }
                else
                {
                    seatsView.CurrentCell.Style.BackColor = Color.DarkTurquoise;
                    seatsView.CurrentCell.Style.SelectionBackColor = Color.DarkTurquoise;
                    Seats.Add(""+seatsView.CurrentCellAddress.Y + "." + seatsView.CurrentCellAddress.X);
                }
            }
        }

        private void bookButton_Click(object sender, EventArgs e)
        {
            movieTitle = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[2].Value.ToString();
            var showID = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[0].Value.ToString();
            if (Name == string.Empty)
            {
                using (var form = new AskName(movieTitle))
                {
                    form.ShowDialog();
                    Name = form.CustomerName;
                }
            }
            if (IssueTicket(showID) == true)
                MessageBox.Show("Done!");
            Close();
        }

        public bool IssueTicket(string showId) 
        {



            var issued = false;
            var divider = (Seats.Count > 0) ? ";" : "";
            var connection = new SqlConnection(connStr);
            var customerId = "";
            var customerPrice = "";
            var customerToPay = 0;

            var getNameId = new SqlCommand(
                "SELECT Id FROM Customers WHERE Customers.Name=@Name"
                , connection);
            getNameId.Parameters.AddWithValue("@Name", Name);

            var getDiscount= new SqlCommand(
                 "SELECT Employers.Discount FROM Customers  "
                 + "JOIN Employers ON Customers.Employer_ID = Employers.Id "
                 + "WHERE Customers.Name='"+ Name + "'"
                 , connection);


            try
            {
                connection.Open();
                customerId = getNameId.ExecuteScalar().ToString();
                customerPrice = getDiscount.ExecuteScalar().ToString();
                customerToPay = Convert.ToByte(customerPrice)*Seats.Count;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }

            var insertTicket = new SqlCommand(
                "INSERT INTO Tickets (Owner, Show_ID, Price)  "  
                // --------------------------------
                //
                // !!! don't forget to check price
                //
                // --------------------------------
                + "OUTPUT INSERTED.Id VALUES(" + customerId +","+ showId + "," + customerToPay + ")"
                , connection);

            var checkSeats = new SqlCommand(
                "SELECT Seats.Seats FROM Seats "
                + "JOIN Shows ON Shows.Seats_ID=Seats.Id "
                + "WHERE Shows.ID=" + showId
                , connection);

            try
            {
                var ticketId = insertTicket.ExecuteScalar();
                var seatsToBook = new StringBuilder();
                foreach (var seat in Seats)
                {
                    seatsToBook.Append(divider + seat + "." + ticketId.ToString());
                    divider = ";";
                }
                var currentSeatsRow = checkSeats.ExecuteScalar();
                if (currentSeatsRow == null)
                {
                    var insertSeats = new SqlCommand(
                    "INSERT INTO Seats (Seats) VALUES('" + seatsToBook.ToString() + "')"
                    , connection);
                    insertSeats.ExecuteNonQuery();                    
                }
                else
                {
                    var newSeat = currentSeatsRow + seatsToBook.ToString();
                    var updateSeats = new SqlCommand(
                    "UPDATE Seats SET Seats='" + newSeat.ToString() + "' WHERE Seats='" + currentSeatsRow +"'"
                    , connection);
                    updateSeats.ExecuteNonQuery();
                }
                issued = true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
            finally
            {
                if (connection != null)
                    connection.Close();

            }
            return issued;
        }

    }
}
