﻿namespace Booking.Booking
{
    partial class CheckSeats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.showGridView = new System.Windows.Forms.DataGridView();
            this.checkButton = new System.Windows.Forms.Button();
            this.seatsPanel = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.backButton = new System.Windows.Forms.Button();
            this.seatsView = new System.Windows.Forms.DataGridView();
            this.s1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s4 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s5 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s6 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s7 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s8 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s9 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s10 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s11 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.s12 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.movieName = new System.Windows.Forms.Label();
            this.bookButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.showGridView)).BeginInit();
            this.seatsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seatsView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Source Sans Pro Light", 16F);
            this.label1.Location = new System.Drawing.Point(83, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Movies";
            // 
            // showGridView
            // 
            this.showGridView.AllowUserToAddRows = false;
            this.showGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.showGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.showGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.showGridView.Location = new System.Drawing.Point(88, 103);
            this.showGridView.Name = "showGridView";
            this.showGridView.Size = new System.Drawing.Size(718, 282);
            this.showGridView.TabIndex = 1;
            // 
            // checkButton
            // 
            this.checkButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.checkButton.Location = new System.Drawing.Point(448, 408);
            this.checkButton.Name = "checkButton";
            this.checkButton.Size = new System.Drawing.Size(160, 37);
            this.checkButton.TabIndex = 2;
            this.checkButton.Text = "Show seats";
            this.checkButton.UseVisualStyleBackColor = true;
            this.checkButton.Click += new System.EventHandler(this.checkButton_Click);
            // 
            // seatsPanel
            // 
            this.seatsPanel.Controls.Add(this.nameLabel);
            this.seatsPanel.Controls.Add(this.backButton);
            this.seatsPanel.Controls.Add(this.seatsView);
            this.seatsPanel.Controls.Add(this.movieName);
            this.seatsPanel.Controls.Add(this.bookButton);
            this.seatsPanel.Location = new System.Drawing.Point(60, 33);
            this.seatsPanel.Name = "seatsPanel";
            this.seatsPanel.Size = new System.Drawing.Size(746, 437);
            this.seatsPanel.TabIndex = 3;
            this.seatsPanel.Visible = false;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Source Sans Pro Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(22, 331);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(110, 27);
            this.nameLabel.TabIndex = 190;
            this.nameLabel.Text = "Booking for";
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.backButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.backButton.ForeColor = System.Drawing.Color.Transparent;
            this.backButton.Location = new System.Drawing.Point(28, 375);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(160, 37);
            this.backButton.TabIndex = 189;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = false;
            this.backButton.Click += new System.EventHandler(this.backlButton_Click);
            // 
            // seatsView
            // 
            this.seatsView.AllowUserToAddRows = false;
            this.seatsView.AllowUserToDeleteRows = false;
            this.seatsView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.seatsView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.seatsView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.seatsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.seatsView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.s1,
            this.s2,
            this.s3,
            this.s4,
            this.s5,
            this.s6,
            this.s7,
            this.s8,
            this.s9,
            this.s10,
            this.s11,
            this.s12});
            this.seatsView.Location = new System.Drawing.Point(28, 84);
            this.seatsView.Margin = new System.Windows.Forms.Padding(2);
            this.seatsView.Name = "seatsView";
            this.seatsView.RowTemplate.Height = 24;
            this.seatsView.Size = new System.Drawing.Size(631, 223);
            this.seatsView.TabIndex = 188;
            this.seatsView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.seatsView_CellContentClick);
            // 
            // s1
            // 
            this.s1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.s1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s1.HeaderText = "1";
            this.s1.Name = "s1";
            this.s1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s1.Width = 35;
            // 
            // s2
            // 
            this.s2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.s2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s2.HeaderText = "2";
            this.s2.MinimumWidth = 2;
            this.s2.Name = "s2";
            this.s2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s2.Width = 35;
            // 
            // s3
            // 
            this.s3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.s3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s3.HeaderText = "3";
            this.s3.Name = "s3";
            this.s3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s3.Width = 35;
            // 
            // s4
            // 
            this.s4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.s4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s4.HeaderText = "4";
            this.s4.Name = "s4";
            this.s4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s4.Width = 35;
            // 
            // s5
            // 
            this.s5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s5.HeaderText = "5";
            this.s5.Name = "s5";
            this.s5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s5.Width = 35;
            // 
            // s6
            // 
            this.s6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s6.HeaderText = "6";
            this.s6.Name = "s6";
            this.s6.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s6.Width = 35;
            // 
            // s7
            // 
            this.s7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s7.HeaderText = "7";
            this.s7.Name = "s7";
            this.s7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s7.Width = 35;
            // 
            // s8
            // 
            this.s8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s8.HeaderText = "8";
            this.s8.Name = "s8";
            this.s8.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s8.Width = 35;
            // 
            // s9
            // 
            this.s9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s9.HeaderText = "9";
            this.s9.Name = "s9";
            this.s9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s9.Width = 35;
            // 
            // s10
            // 
            this.s10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s10.HeaderText = "10";
            this.s10.Name = "s10";
            this.s10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s10.Width = 35;
            // 
            // s11
            // 
            this.s11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s11.HeaderText = "11";
            this.s11.Name = "s11";
            this.s11.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s11.Width = 35;
            // 
            // s12
            // 
            this.s12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.s12.HeaderText = "12";
            this.s12.Name = "s12";
            this.s12.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.s12.Width = 35;
            // 
            // movieName
            // 
            this.movieName.AutoSize = true;
            this.movieName.Font = new System.Drawing.Font("Source Sans Pro Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.movieName.Location = new System.Drawing.Point(22, 43);
            this.movieName.Name = "movieName";
            this.movieName.Size = new System.Drawing.Size(114, 27);
            this.movieName.TabIndex = 187;
            this.movieName.Text = "Movie name";
            // 
            // bookButton
            // 
            this.bookButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bookButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.bookButton.Location = new System.Drawing.Point(388, 375);
            this.bookButton.Name = "bookButton";
            this.bookButton.Size = new System.Drawing.Size(160, 37);
            this.bookButton.TabIndex = 4;
            this.bookButton.Text = "Book";
            this.bookButton.UseVisualStyleBackColor = true;
            this.bookButton.Click += new System.EventHandler(this.bookButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.cancelButton.ForeColor = System.Drawing.Color.Transparent;
            this.cancelButton.Location = new System.Drawing.Point(87, 408);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(161, 37);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // CheckSeats
            // 
            this.AcceptButton = this.bookButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CancelButton = this.backButton;
            this.ClientSize = new System.Drawing.Size(854, 510);
            this.ControlBox = false;
            this.Controls.Add(this.seatsPanel);
            this.Controls.Add(this.checkButton);
            this.Controls.Add(this.showGridView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CheckSeats";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "BookSeat";
            this.Load += new System.EventHandler(this.BookSeat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.showGridView)).EndInit();
            this.seatsPanel.ResumeLayout(false);
            this.seatsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seatsView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView showGridView;
        private System.Windows.Forms.Button checkButton;
        private System.Windows.Forms.Panel seatsPanel;
        private System.Windows.Forms.Button bookButton;
        private System.Windows.Forms.Label movieName;
        private System.Windows.Forms.DataGridView seatsView;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.DataGridViewButtonColumn s1;
        private System.Windows.Forms.DataGridViewButtonColumn s2;
        private System.Windows.Forms.DataGridViewButtonColumn s3;
        private System.Windows.Forms.DataGridViewButtonColumn s4;
        private System.Windows.Forms.DataGridViewButtonColumn s5;
        private System.Windows.Forms.DataGridViewButtonColumn s6;
        private System.Windows.Forms.DataGridViewButtonColumn s7;
        private System.Windows.Forms.DataGridViewButtonColumn s8;
        private System.Windows.Forms.DataGridViewButtonColumn s9;
        private System.Windows.Forms.DataGridViewButtonColumn s10;
        private System.Windows.Forms.DataGridViewButtonColumn s11;
        private System.Windows.Forms.DataGridViewButtonColumn s12;
    }
}