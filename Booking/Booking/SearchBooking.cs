﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Documents;
using System.Windows.Forms;

namespace Booking.Booking
{
    public partial class SearchBooking : Form
    {
        public SearchBooking()
        {
            InitializeComponent();
            gridPanel.Visible = false;
        }
        public string connStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;



        private void SubmitSearch_Click(object sender, EventArgs e)
        {
            panelAddNew.Visible = false;
            gridPanel.Visible = false;
            DataTable dt = new DataTable(connStr);
            SqlConnection connection = new SqlConnection(connStr);
            SqlCommand check = new SqlCommand("SELECT Name FROM Customers WHERE Name = @Name", connection);
            SqlCommand cmd = new SqlCommand(
                "SELECT Tickets.Id, Shows.Id, Movies.Title, Shows.Date, Tickets.Price, Tickets.Paid "
                +"FROM Shows JOIN Movies ON Shows.Movie_ID = Movies.Id " 
                +"JOIN Tickets ON Tickets.Show_ID = Shows.Id "
                +"JOIN Seats ON Shows.Seats_ID = Seats.Id "
                +"JOIN Customers ON Customers.Id = Tickets.Owner " 
                +"WHERE Shows.Date >= GETDATE() AND Customers.Name=@Name"
                , connection);

            cmd.Parameters.AddWithValue("@Name", ((object)SearchBox.Text ?? DBNull.Value));
            check.Parameters.AddWithValue("@Name", ((object)SearchBox.Text ?? DBNull.Value));

            try
            {
                connection.Open();

                var name = check.ExecuteScalar();
                if (name == null)
                    MessageBox.Show("No such customer!");
                else
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        dt.Load(reader);
                        ticketsGridView.DataSource = dt;
                        ticketsGridView.Columns[1].Visible = false;
                        ticketsGridView.Columns[0].HeaderText = "Ticket №";
                        ticketsGridView.Columns[2].Width = 278;
                        ticketsGridView.Columns[1].ReadOnly = true;
                        ticketsGridView.Columns[2].ReadOnly = true;
                        ticketsGridView.Columns[3].ReadOnly = true;
                        ticketsGridView.Columns[4].ReadOnly = true;
                        gridPanel.Visible = true; 
                    }
                    else
                        panelAddNew.Visible = true;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
            finally
            {
                if(connection != null) { }
                    connection.Close();
            }
        }



        private void SearchLabel_Click(object sender, EventArgs e)
        {

        }

        private void SearchBooking_Load(object sender, EventArgs e)
        {
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(connStr);

            var ticketID = ticketsGridView.Rows[ticketsGridView.CurrentCell.RowIndex].Cells[0].Value;
            try
            {
                var sql = " UPDATE Tickets SET Paid=@Paid WHERE Id=" + ticketID.ToString();
                SqlCommand cmd = new SqlCommand(sql, connection);

                var ticketPaid = ticketsGridView.Rows[ticketsGridView.CurrentCell.RowIndex].Cells[5].Value.ToString();

                cmd.Parameters.AddWithValue("@Paid", ticketPaid);

                connection.Open();
                cmd.ExecuteNonQuery();
 
            }
            catch (InvalidCastException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(connStr);

            var ticketID = ticketsGridView.Rows[ticketsGridView.CurrentCell.RowIndex].Cells[0].Value;
            MessageBox.Show(ticketID.ToString());
            try
            {
                var sql = "DELETE FROM Tickets WHERE Tickets.Id=" + ticketID + "";
                SqlCommand cmd = new SqlCommand(sql, connection);
                connection.Open(); 
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            Form form = new CheckSeats(SearchBox.Text);
            this.Hide();
            form.MdiParent = this.ParentForm;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void bookNowButton_Click(object sender, EventArgs e)
        {
            Form form = new CheckSeats(SearchBox.Text);
            this.Hide();
            form.MdiParent = this.ParentForm;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
