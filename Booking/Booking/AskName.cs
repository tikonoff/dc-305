﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Booking.Booking
{
    public partial class AskName : Form
    {
        public string CustomerName => Customer;

        private string Customer;

        public AskName(string title)
        {
            InitializeComponent();
            namesPanel.Visible = false;
            confirmLabel.Text = title;
        }

        public string connStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;

        private void findButton_Click(object sender, EventArgs e)
        {
            var connection = new SqlConnection(connStr);
            var dt = new DataTable(connStr);
            var cmd = new SqlCommand(
                "SELECT Customers.Id, Customers.Name, Employers.Name FROM Customers " +
                "JOIN Employers ON Customers.Employer_ID = Employers.Id " +
                "WHERE  Customers.Name LIKE '%" + nameBox.Text + "%'"
                , connection);

            try
            {
                connection.Open();
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dt.Load(reader);
                    namesView.DataSource = dt;
                    namesView.Columns[0].Visible = false;
                    namesView.Columns[1].HeaderText = "Name";
                    namesView.Columns[1].Width = 200;
                    namesView.Columns[2].HeaderText = "Company";
                    namesView.Columns[2].Width = 118;
                    namesPanel.Visible = true;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            Customer = namesView.Rows[namesView.CurrentCell.RowIndex].Cells[1].Value.ToString();
            Close();
        }
    }
}
 