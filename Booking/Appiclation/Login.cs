﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Navigation;
using Application = System.Windows.Forms.Application;
using MessageBox = System.Windows.MessageBox;

namespace Booking
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }
        public int loginAttempts = 3;
        private string connStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;

        private void password_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void username_Click(object sender, EventArgs e)
        {

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(connStr);
            SqlCommand validateUser = new SqlCommand(
                "SELECT * FROM Users WHERE UserName=@User AND Password=@Password"
                , connection);

            validateUser.Parameters.AddWithValue("@User", ((object)usernameBox.Text) ?? DBNull.Value);
            validateUser.Parameters.AddWithValue("@Password", ((object)passwordBox.Text) ?? DBNull.Value);

            connection.Open();
            var reader = validateUser.ExecuteReader();
            var userLogged = "";

            if (reader.Read())
            { 
                userLogged = (string) reader[1];
                MainForm newForm = new MainForm(userLogged);
                this.Hide();
                newForm.ShowDialog();
                this.Close();
            }
            else
            {
                if (--loginAttempts == 0)
                    this.Close();
                MessageBox.Show("Attempts remain:" + loginAttempts);
            }

            passwordBox.Clear();
            usernameBox.Clear();
            connection.Close();

        }
          

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
            System.Windows.Forms.Application.Exit();

        }

        private void passwordBox_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
