﻿namespace Booking
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.usernameBox = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.passwordBox = new System.Windows.Forms.MaskedTextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.windowTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // usernameBox
            // 
            this.usernameBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usernameBox.Font = new System.Drawing.Font("Segoe UI Light", 16F);
            this.usernameBox.Location = new System.Drawing.Point(242, 184);
            this.usernameBox.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.usernameBox.Name = "usernameBox";
            this.usernameBox.Size = new System.Drawing.Size(323, 29);
            this.usernameBox.TabIndex = 0;
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("Segoe UI Light", 16F);
            this.username.ForeColor = System.Drawing.Color.FloralWhite;
            this.username.Location = new System.Drawing.Point(117, 184);
            this.username.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(162, 58);
            this.username.TabIndex = 1;
            this.username.Text = "User Name";
            this.username.Click += new System.EventHandler(this.username_Click);
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Segoe UI Light", 16F);
            this.password.ForeColor = System.Drawing.Color.FloralWhite;
            this.password.Location = new System.Drawing.Point(117, 227);
            this.password.Margin = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(188, 45);
            this.password.TabIndex = 3;
            this.password.Text = "Password";
            this.password.Click += new System.EventHandler(this.password_Click);
            // 
            // passwordBox
            // 
            this.passwordBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.passwordBox.Font = new System.Drawing.Font("Segoe UI Light", 16F);
            this.passwordBox.Location = new System.Drawing.Point(242, 227);
            this.passwordBox.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.Size = new System.Drawing.Size(323, 29);
            this.passwordBox.TabIndex = 4;
            this.passwordBox.UseSystemPasswordChar = true;
            this.passwordBox.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.passwordBox_MaskInputRejected);
            // 
            // okButton
            // 
            this.okButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.okButton.FlatAppearance.BorderSize = 2;
            this.okButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.okButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.okButton.Font = new System.Drawing.Font("Segoe UI Light", 16F);
            this.okButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.okButton.Location = new System.Drawing.Point(242, 275);
            this.okButton.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(155, 50);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.cancelButton.FlatAppearance.BorderSize = 3;
            this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Segoe UI Light", 16F);
            this.cancelButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cancelButton.Location = new System.Drawing.Point(418, 275);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(44, 41, 44, 41);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(147, 49);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // windowTitle
            // 
            this.windowTitle.AutoSize = true;
            this.windowTitle.BackColor = System.Drawing.Color.Transparent;
            this.windowTitle.Font = new System.Drawing.Font("Segoe UI Light", 32F);
            this.windowTitle.ForeColor = System.Drawing.Color.Gold;
            this.windowTitle.Location = new System.Drawing.Point(112, 91);
            this.windowTitle.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.windowTitle.Name = "windowTitle";
            this.windowTitle.Size = new System.Drawing.Size(470, 59);
            this.windowTitle.TabIndex = 8;
            this.windowTitle.Text = "Golden Grammy Cinema";
            this.windowTitle.Click += new System.EventHandler(this.label1_Click);
            // 
            // LoginForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(698, 434);
            this.Controls.Add(this.windowTitle);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.usernameBox);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(12, 11, 12, 11);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Booka-tiki";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox usernameBox;
        private System.Windows.Forms.Label username;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.MaskedTextBox passwordBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label windowTitle;
    }
}

