﻿namespace Booking
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TicketsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.showMoviesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findBookingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.MovieMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.addMovie = new System.Windows.Forms.ToolStripMenuItem();
            this.schedule = new System.Windows.Forms.ToolStripMenuItem();
            this.MovieStatistic = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sampleFormBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TicketsMenu,
            this.exitMenu,
            this.MovieMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(1219, 87);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // TicketsMenu
            // 
            this.TicketsMenu.BackColor = System.Drawing.Color.Transparent;
            this.TicketsMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.TicketsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showMoviesToolStripMenuItem,
            this.findBookingToolStripMenuItem});
            this.TicketsMenu.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TicketsMenu.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TicketsMenu.Name = "TicketsMenu";
            this.TicketsMenu.Padding = new System.Windows.Forms.Padding(20);
            this.TicketsMenu.Size = new System.Drawing.Size(145, 85);
            this.TicketsMenu.Text = "Tickets";
            this.TicketsMenu.Click += new System.EventHandler(this.showMenu_Click);
            // 
            // showMoviesToolStripMenuItem
            // 
            this.showMoviesToolStripMenuItem.Font = new System.Drawing.Font("Source Sans Pro Light", 14F);
            this.showMoviesToolStripMenuItem.Name = "showMoviesToolStripMenuItem";
            this.showMoviesToolStripMenuItem.Size = new System.Drawing.Size(225, 36);
            this.showMoviesToolStripMenuItem.Text = "Book now";
            this.showMoviesToolStripMenuItem.Click += new System.EventHandler(this.showMoviesToolStripMenuItem_Click);
            // 
            // findBookingToolStripMenuItem
            // 
            this.findBookingToolStripMenuItem.Font = new System.Drawing.Font("Source Sans Pro Light", 14F);
            this.findBookingToolStripMenuItem.Name = "findBookingToolStripMenuItem";
            this.findBookingToolStripMenuItem.Size = new System.Drawing.Size(225, 36);
            this.findBookingToolStripMenuItem.Text = "Find booking";
            this.findBookingToolStripMenuItem.Click += new System.EventHandler(this.findBookingToolStripMenuItem_Click);
            // 
            // exitMenu
            // 
            this.exitMenu.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.exitMenu.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitMenu.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.exitMenu.Name = "exitMenu";
            this.exitMenu.Padding = new System.Windows.Forms.Padding(20);
            this.exitMenu.Size = new System.Drawing.Size(105, 85);
            this.exitMenu.Text = "Exit";
            this.exitMenu.Click += new System.EventHandler(this.exitMenu_Click);
            // 
            // MovieMenu
            // 
            this.MovieMenu.BackColor = System.Drawing.Color.Transparent;
            this.MovieMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.MovieMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMovie,
            this.schedule,
            this.MovieStatistic});
            this.MovieMenu.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MovieMenu.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.MovieMenu.Name = "MovieMenu";
            this.MovieMenu.Padding = new System.Windows.Forms.Padding(20);
            this.MovieMenu.Size = new System.Drawing.Size(151, 85);
            this.MovieMenu.Text = "Movies";
            this.MovieMenu.Click += new System.EventHandler(this.MovieMenu_Click);
            // 
            // addMovie
            // 
            this.addMovie.Font = new System.Drawing.Font("Source Sans Pro Light", 14F);
            this.addMovie.Name = "addMovie";
            this.addMovie.Size = new System.Drawing.Size(269, 36);
            this.addMovie.Text = "Add new";
            this.addMovie.Click += new System.EventHandler(this.addMovie_Click);
            // 
            // schedule
            // 
            this.schedule.Font = new System.Drawing.Font("Source Sans Pro Light", 14F);
            this.schedule.Name = "schedule";
            this.schedule.Size = new System.Drawing.Size(269, 36);
            this.schedule.Text = "Change schedule";
            this.schedule.Click += new System.EventHandler(this.schedule_Click);
            // 
            // MovieStatistic
            // 
            this.MovieStatistic.Font = new System.Drawing.Font("Source Sans Pro Light", 14F);
            this.MovieStatistic.Name = "MovieStatistic";
            this.MovieStatistic.Size = new System.Drawing.Size(269, 36);
            this.MovieStatistic.Text = "View statistic";
            this.MovieStatistic.Click += new System.EventHandler(this.MovieStatistic_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(1219, 676);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cinema Booking system";
            this.Load += new System.EventHandler(this.displayMovies_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sampleFormBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TicketsMenu;
        private System.Windows.Forms.ToolStripMenuItem exitMenu;
        private System.Windows.Forms.BindingSource sampleFormBindingSource;
        private System.Windows.Forms.ToolStripMenuItem MovieMenu;
        private System.Windows.Forms.ToolStripMenuItem MovieStatistic;
        private System.Windows.Forms.ToolStripMenuItem addMovie;
        private System.Windows.Forms.ToolStripMenuItem showMoviesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findBookingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem schedule;
    }
}