﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;
using Booking.Booking;
using Booking.Management;

namespace Booking
{
    public partial class MainForm : Form
    {
        public MainForm(string adminLogged)
        {
            InitializeComponent();
            if (adminLogged != "admin")
                MovieMenu.Visible = false;

        }

    

        private void exitMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void displayMovies_Load(object sender, EventArgs e)
        {
        }

        private void showMenu_Click(object sender, EventArgs e)
        {

        }


        private void addMovie_Click(object sender, EventArgs e)
        {
            Form newForm = new FormAddMovie();
            newForm.MdiParent = this;
            newForm.WindowState = FormWindowState.Maximized;
            newForm.Show();
        }

        private void MovieMenu_Click(object sender, EventArgs e)
        {

        }

        private void ViewMovies_Click(object sender, EventArgs e)
        {
            Form form = new CheckSeats();
            form.MdiParent = this;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void findBookingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new SearchBooking();
            form.MdiParent = this;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void showMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new CheckSeats();
            form.MdiParent = this;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void schedule_Click(object sender, EventArgs e)
        {
            Form form = new Schedule();
            form.MdiParent = this;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }

        private void MovieStatistic_Click(object sender, EventArgs e)
        {
            Form form = new FormShowStatistic();
            form.MdiParent = this;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }
    }
}
