﻿namespace Booking.Management
{
    partial class FormAddMovie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleBox = new System.Windows.Forms.TextBox();
            this.title = new System.Windows.Forms.Label();
            this.actorBox = new System.Windows.Forms.ListBox();
            this.typeBox = new System.Windows.Forms.ListBox();
            this.ratingBox = new System.Windows.Forms.ComboBox();
            this.rating = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleBox
            // 
            this.titleBox.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.titleBox.Location = new System.Drawing.Point(82, 86);
            this.titleBox.Name = "titleBox";
            this.titleBox.Size = new System.Drawing.Size(261, 28);
            this.titleBox.TabIndex = 0;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.title.Location = new System.Drawing.Point(81, 58);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(38, 20);
            this.title.TabIndex = 1;
            this.title.Text = "Title";
            // 
            // actorBox
            // 
            this.actorBox.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.actorBox.FormattingEnabled = true;
            this.actorBox.ItemHeight = 20;
            this.actorBox.Location = new System.Drawing.Point(82, 143);
            this.actorBox.Name = "actorBox";
            this.actorBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.actorBox.Size = new System.Drawing.Size(261, 164);
            this.actorBox.Sorted = true;
            this.actorBox.TabIndex = 3;
            this.actorBox.SelectedIndexChanged += new System.EventHandler(this.actorBox_SelectedIndexChanged);
            // 
            // typeBox
            // 
            this.typeBox.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.typeBox.FormattingEnabled = true;
            this.typeBox.ItemHeight = 20;
            this.typeBox.Location = new System.Drawing.Point(366, 143);
            this.typeBox.Name = "typeBox";
            this.typeBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.typeBox.Size = new System.Drawing.Size(168, 164);
            this.typeBox.Sorted = true;
            this.typeBox.TabIndex = 2;
            this.typeBox.SelectedIndexChanged += new System.EventHandler(this.typeBox_SelectedIndexChanged);
            // 
            // ratingBox
            // 
            this.ratingBox.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.ratingBox.FormattingEnabled = true;
            this.ratingBox.Location = new System.Drawing.Point(366, 86);
            this.ratingBox.Name = "ratingBox";
            this.ratingBox.Size = new System.Drawing.Size(168, 28);
            this.ratingBox.TabIndex = 6;
            // 
            // rating
            // 
            this.rating.AutoSize = true;
            this.rating.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.rating.Location = new System.Drawing.Point(361, 58);
            this.rating.Name = "rating";
            this.rating.Size = new System.Drawing.Size(52, 20);
            this.rating.TabIndex = 7;
            this.rating.Text = "Rating";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Source Sans Pro Light", 12F);
            this.button1.Location = new System.Drawing.Point(383, 357);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 41);
            this.button1.TabIndex = 11;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelButton.Font = new System.Drawing.Font("Source Sans Pro Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.ForeColor = System.Drawing.Color.Transparent;
            this.cancelButton.Location = new System.Drawing.Point(82, 357);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(163, 41);
            this.cancelButton.TabIndex = 16;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // addMovie
            // 
            this.ClientSize = new System.Drawing.Size(940, 500);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ratingBox);
            this.Controls.Add(this.actorBox);
            this.Controls.Add(this.typeBox);
            this.Controls.Add(this.title);
            this.Controls.Add(this.titleBox);
            this.Controls.Add(this.rating);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAddMovie";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.TextBox titleBox;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.ListBox actorBox;
        private System.Windows.Forms.ListBox typeBox;
        private System.Windows.Forms.ComboBox ratingBox;
        private System.Windows.Forms.Label rating;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button cancelButton;
    }
}