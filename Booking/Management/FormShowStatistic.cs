﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Booking.Management
{
    public partial class FormShowStatistic : Form
    {
        public FormShowStatistic()
        {
            InitializeComponent();
            var dt = new DataTable();
            var da = (SqlDataAdapter) Movie.ShowPopular();
            da.Fill(dt);
            typesGridView.DataSource = dt;
            typesGridView.Columns[0].HeaderText = "Type";
            typesGridView.Columns[1].HeaderText = "Box office";
            typesGridView.Columns[1].DefaultCellStyle.Format = "c";
        }
    }
}
