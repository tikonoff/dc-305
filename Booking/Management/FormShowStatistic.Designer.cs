﻿namespace Booking.Management
{
    partial class FormShowStatistic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.typesGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.typesGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // typesGridView
            // 
            this.typesGridView.AllowUserToAddRows = false;
            this.typesGridView.AllowUserToDeleteRows = false;
            this.typesGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.typesGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.typesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.typesGridView.Location = new System.Drawing.Point(71, 133);
            this.typesGridView.Name = "typesGridView";
            this.typesGridView.ReadOnly = true;
            this.typesGridView.RowTemplate.Height = 24;
            this.typesGridView.Size = new System.Drawing.Size(733, 232);
            this.typesGridView.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Source Sans Pro Light", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(64, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 36);
            this.label1.TabIndex = 2;
            this.label1.Text = "Popular movie types";
            // 
            // Statistic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 534);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.typesGridView);
            this.Name = "Statistic";
            this.ShowInTaskbar = false;
            this.Text = "Statistic";
            ((System.ComponentModel.ISupportInitialize)(this.typesGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView typesGridView;
        private System.Windows.Forms.Label label1;
    }
}