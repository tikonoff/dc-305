﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Booking.Management;

namespace Booking.Management
{
    public partial class FormAddMovie : Form
    {
        public int MovieId { get; set; }

        public FormAddMovie()
        {
            InitializeComponent();
            LoadTables();
        }
        public readonly string ConnStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;


        private bool LoadTables()
        {
            SqlConnection connection = new SqlConnection(ConnStr);
 
            try
            {
                connection.Open();
                var cmd = new SqlCommand("SELECT Name FROM Actors", connection);
                var da = new SqlDataAdapter(cmd);
                var dt = new DataTable();
                da.Fill(dt);
                actorBox.DataSource = dt;
                actorBox.DisplayMember = "Name";

                cmd = new SqlCommand("SELECT Name FROM Types", connection);
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                typeBox.DataSource = dt;
                typeBox.DisplayMember = "Name";

                cmd = new SqlCommand("SELECT Name FROM Ratings", connection);
                da = new SqlDataAdapter(cmd);
                dt = new DataTable();
                da.Fill(dt);
                ratingBox.DataSource = dt;
                ratingBox.DisplayMember = "Name";
                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
                return false;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

        }

        private void typeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            typeBox.SelectionMode = SelectionMode.MultiExtended;
        }

        private void actorBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            actorBox.SelectionMode = SelectionMode.MultiExtended;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (titleBox.Text != "")
            {
                var newMovie = new Movie(titleBox.Text, typeBox.Text, actorBox.Text, ratingBox.Text);
                var mId = newMovie.Add();
                if ( mId <= 0) return;
                MessageBox.Show("Movie №"+mId+" added.");
                title.Text = "";
                typeBox.Text = "";
                actorBox.Text = "";
                ratingBox.Text = "";
            }
            else
                MessageBox.Show("Enter Title, at least!");

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

     }
}
