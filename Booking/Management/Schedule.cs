﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Booking.Management
{
    public partial class Schedule : Form
    {
        public Schedule()
        {
            InitializeComponent();
            Show();
        }

        public string connStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        

        void Show() { 
            var dt = new DataTable(connStr);
            var connection = new SqlConnection(connStr);
            var currentShow = (checkBox.Checked == true) 
                ? "WHERE Shows.Date IS NULL" : "WHERE Shows.Date >= GETDATE() ";

            var cmd = new SqlCommand(@"
                SELECT Shows.Id, Shows.Date, Movies.Title, Movies.Id FROM Movies 
                full join Shows On Movies.Id = Shows.Movie_ID
                " + currentShow, connection);

            try
            {
                connection.Open();
                var reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    dt.Load(reader);
                    showGridView.DataSource = dt;
                    showGridView.Columns[1].Width = 100;
                    showGridView.Columns[2].Width = 375;
                    showGridView.Columns[0].Visible = false;
                    showGridView.Columns[3].Visible = false;
                    showGridView.Columns[1].ReadOnly = true;
                    showGridView.Columns[2].ReadOnly = true;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);

            }
            finally
            {
                if (connection != null)
                    connection.Close();
             }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            var showId = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[0].Value;
            var movieId = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[3].Value;
            var connection = new SqlConnection(connStr);
            SqlCommand cmd;
            if (showId.ToString() == "")
            {
                cmd = new SqlCommand("INSERT INTO Shows (Date, Movie_ID) VALUES (@Date,@Movie_ID)", connection);
                cmd.Parameters.AddWithValue("@Date", showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[1].Value);
                cmd.Parameters.AddWithValue("@Movie_ID", movieId);
            }
            else
            {
                cmd = new SqlCommand("UPDATE Shows SET Date=@Date WHERE Id=@Show_ID", connection);
                cmd.Parameters.AddWithValue("@Date", showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[1].Value);
                cmd.Parameters.AddWithValue("@Show_ID", showId);
            }

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
                this.showGridView.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                Show();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var showId = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[0].Value;
            var connection = new SqlConnection(connStr);
            SqlCommand cmd;
            try
            {
                connection.Open();
                if (showId.ToString() == "")
                {
                    var movieId = showGridView.Rows[showGridView.CurrentCell.RowIndex].Cells[3].Value;
                    cmd = new SqlCommand("DELETE FROM Movies WHERE Id=" + movieId, connection);
                }
                else
                {
                    cmd = new SqlCommand("DELETE FROM Shows WHERE Id=" + showId, connection);
                }

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("This show already has tickets sold. Can't delete.");
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                Show();
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            Show();
        }

        private void calendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            showGridView.Rows[showGridView.CurrentRow.Index].Cells[1].Value = calendar.SelectionEnd;
                
        }
    }
}