﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Booking.Management
{
    public class Movie
    {
        public static readonly string ConnStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        public string Title;
        public string Type;
        public string Actor;
        public string Rating;

        public Movie(string title, string type, string actor, string rating)
        {
            Title = title;
            Type = type;
            Actor = actor;
            Rating = rating;
        }

        public int Add()
        {
            int movieId;
            SqlConnection connection = new SqlConnection(ConnStr);
            SqlCommand saveMovie = new SqlCommand(@"
	                INSERT INTO Movies (Title, Actor_ID, Type_ID, Rating_ID) 
	                OUTPUT INSERTED.Id VALUES (@Title, 
                        (SELECT Id FROM Actors WHERE Name=@Actor), 
                        (SELECT Id FROM Types WHERE Name=@Type), 
                        (SELECT Id FROM Ratings WHERE Name=@Rating)
                        )
                    ", connection);

            saveMovie.Parameters.AddWithValue("@Title", Title);
            saveMovie.Parameters.AddWithValue("@Type", Type);
            saveMovie.Parameters.AddWithValue("@Actor", Actor);
            saveMovie.Parameters.AddWithValue("@Rating", Rating);

            try
            {
                connection.Open();
                movieId = (int)saveMovie.ExecuteScalar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                movieId = 0;
            }
            finally
            {
                connection.Close();
            }
            return movieId;
        }

        public static object ShowPopular()
        {
            SqlConnection connection = new SqlConnection(ConnStr);
            SqlDataAdapter da;
            var sql = @"
                SELECT Name, SUM(Tickets.Price) FROM Types
                JOIN Movies ON Movies.Type_ID = Types.Id
                JOIN Shows ON Movie_ID = Movies.Id
                JOIN Tickets ON Tickets.Show_ID = Shows.Id
				GROUP BY Types.Name
				ORDER BY SUM(Tickets.Price) DESC
                ";
            var cmd = new SqlCommand(sql, connection);


            try
            {
                connection.Open();
                da = new SqlDataAdapter(cmd);
                return da;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
        }
    }
}
